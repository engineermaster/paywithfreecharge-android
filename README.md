# FREECHARGE PAYMENTS ANDROID SDK  #


### ENVIRONMENT AND BUILD TOOL ###

####IDE####
 * ANDROID STUDIO

####BUILD TOOL####
* GRADLE

###INTEGRATION STEPS###

####Follow the steps to integrate Freecharge Payment SDK:####

 1) Add the following compile dependency in build.gradle file of your project. 
 
 
      repositories {
        jcenter()
        maven { 
         url "https://s3-ap-southeast-1.amazonaws.com/godel-release/godel/" 
       }
    }


2) Add the following dependency in build.gradle file of app.

      compile 'com.android.support:support-v4:+'
      compile 'in.freecharge.checkout.android:freecharge-checkout-android-sdk:2.1@aar'
      compile 'in.juspay:godel:0.6.12.1423'



3) Add following permission in your manifest file.

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
    <uses-permission android:name="android.permission.RECEIVE_SMS" />
    <uses-permission android:name="android.permission.READ_SMS" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE"/>

4) Build your project. Now you can import in.freecharge.checkout.FreeChargePaymentSdk. 

5) Initialize SDK in your application class depending on the mode.

####sandbox mode (for testing)####

        FreeChargePaymentSdk.init(this, FreechargeSdkEnvironment.SANDBOX);


####production mode####

        FreeChargePaymentSdk.init(this, FreechargeSdkEnvironment.PRODUCTION);

 Freecharge SDK is integrated completely.

### FREECHARGE SDK SOLUTIONS.

Currently freecharge sdk is packed with two solutions. One can Opt any of them acc to their requirement.

* CHECKOUT SOLUTION
* ADD MONEY SOLUTION

###CHECKOUT SOLUTION INTEGRATION ###

###STEP 1. ###
Add the following callback in your activity or fragment. The callback is 
   used mainly to handle the result of transaction status.

      final FreeChargePaymentCallback freeChargePaymentCallback = new FreeChargePaymentCallback() {

            @Override
            public void onTransactionFailed(HashMap<String, String> txnFailResponse) {
                Toast.makeText(getActivity().getApplicationContext(), txnFailResponse.get("errorMessage"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTransactionCancelled() {
                Toast.makeText(getActivity().getApplicationContext(), "user cancelled the transaction", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTransactionSucceeded(HashMap<String, String> txnSuccessResponse) {
                Toast.makeText(getActivity().getApplicationContext(),txnSuccessResponse.get("status"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onErrorOccurred(FreechargeSdkException sdkError) {
                Toast.makeText(getActivity().getApplicationContext(), sdkError.getErrorMessage(), Toast.LENGTH_SHORT).show();
            }
        };





### Anonymous object of freechargePaymentCallback has basically four functions.###
 
 * onTransactionFailed(HashMap<String, String> txnFailResponse)
  
  it is callbacked by sdk  when transaction status is failed with a hashmap 
  containing following response params.
  
        "errorCode":"E004" 
        "errorMessage":"Duplicate merchant transaction id" 
        "merchantTxnId":"14671120924656"
        "status":"FAILED" 
        "metadata":null
        "authCode":"9HyotbB8sC0bgwDqavnThFv_Zt"
        "checksum":"f246d6fe2e9cbb4818ba931acc51db5626920b4ce62d
  



 * onTransactionSucceeded(HashMap<String, String> txnSuccessResponse)

 This method is callbacked by sdk when transaction status is success with hashmap as transaction response which contains following response params

      "txnId":"VDtyTVmDnufZ3o_146711209246563737378_1"
       "merchantTxnId":"146711209246563737378"
       "amount":"10"
       "metadata":null
       "status":"COMPLETED"
       "authCode":"9HyotbB8sC0bgwDqavnThPas11cW3z6cfXwZY_"
       "checksum":"c176c022235304c30ea4b6e3dd5f6b72331c3"

*  onTransactionCancelled() 

        This method is call backed when user press the back button and agrees to abort the transaction.


*  onErrorOccurred(FreechargeSdkException sdkError)

        This method is call backed when there is some issue in request parameters or sdk has not been initialized.


###STEP 2. ###
        
 Now add the following function in your class to start the payment process by passing request hashmap in init.

        FreeChargePaymentSdk.startSafePayment(getActivity(), checkoutRequestMap, freeChargePaymentCallback);

####"checkoutRequestMap" should be a valid hashmap and it's format mentioned below####



###REQUEST HASHMAP PARAMETERS FOR START SAFE PAYMENT###
 
        HashMap<String, String> checkoutRequestMap = new HashMap<>();
        checkoutRequestMap.put("merchantId","Your Merchant id");
        checkoutRequestMap.put("merchantTxnId","transaction id");
        checkoutRequestMap.put("amount", "transaction amount");
        checkoutRequestMap.put("furl", "fail url (if hosted else put dummy url)");
        checkoutRequestMap.put("surl", "success url (if hosted else put dummy url)");
        checkoutRequestMap.put("amount", "transaction amount");
        checkoutRequestMap.put("productInfo", "auth");
        checkoutRequestMap.put("customerName", "customer/user name if available");
        checkoutRequestMap.put("email", "customer/user email if available");
        checkoutRequestMap.put("mobile",  "customer/user mobile if available");
        checkoutRequestMap.put("channel", "ANDROID");
        checkoutRequestMap.put("checksum", "Generated from merchant backend");
    

Checkout solution integration is completed successfully.


###ADDMONEY SOLUTION INTEGRATION ###

###STEP 1. ###

Add the following callback in your activity or fragment. The callback is 
   used mainly to handle the result of transaction status.

        final FreeChargeAddMoneyCallback freeChargeAddMoneyCallback = new FreeChargeAddMoneyCallback() {

            @Override
            public void onTransactionFailed(HashMap<String, String> txnFailResponse) {
                Toast.makeText(getApplicationContext(), txnFailResponse.get("errorMessage"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTransactionCancelled() {
                Toast.makeText(getApplicationContext(), "user cancelled the transaction", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTransactionSucceeded(HashMap<String, String> txnSuccessResponse) {
                Toast.makeText(getApplicationContext(),txnSuccessResponse.get("status"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onErrorOccurred(FreechargeSdkException sdkError) {
                Toast.makeText(getApplicationContext(), sdkError.getErrorMessage(), Toast.LENGTH_SHORT).show();
            }
        };
        
        
   
### Anonymous object of freeChargeAddMoneyCallback has basically four functions.###
 
 * onTransactionFailed(HashMap<String, String> txnFailResponse)
  
  it is callbacked by sdk  when transaction status is failed with a hashmap 
  containing following response params.
  
        "errorMessage":"Invalid login token"
        "amount":"1"
        "status":"FAILED"
        "errorCode":"E805"
        "checksum":"7d66c67a45e45238f18d608060d63abe28f96b9e414f00735fbf797c298dad13"
  



 * onTransactionSucceeded(HashMap<String, String> txnSuccessResponse)

 This method is callbacked by sdk when transaction status is success with hashmap as transaction response which contains following response params

          "errorMessage":"No error in processing request"
           "walletBalance":"0.00"
           "status":"COMPLETED"
           "errorCode":"E000"
           "checksum":"7e86fdd6ef2d8d2d660c10a696329065abc32ebdd37f689678b3de3136b0c6ff"
            "metadata":"25890"

*  onTransactionCancelled() 

        This method is call backed when user press the back button and agrees to abort the transaction.


*  onErrorOccurred(FreechargeSdkException sdkError)

        This method is call backed when there is some issue in request parameters or sdk has not been initialized.


###STEP 2. ###
        
 Now add the following function in your class to start the payment process by passing request hashmap in init.

        FreeChargePaymentSdk.addMoney(getActivity(), addMoneyRequestHashMap, freeChargeAddMoneyCallback);

####"addMoneyRequestHashMap" should be a valid hashmap and it's format mentioned below####



###REQUEST HASHMAP PARAMETERS FOR ADD MONEY IS MENTIONED BELOW### 
        
        HashMap<String, String> requestHashMap = new HashMap<>();
        requestHashMap.put("merchantId", "Your Merchant id");
        requestHashMap.put("amount","transaction amount);
        requestHashMap.put("channel", "ANDROID");
        requestHashMap.put("callbackUrl","https://test.com your callback url or a dummy url));
        requestHashMap.put("loginToken", "login token of user");
        requestHashMap.put("metadata","your meta data");
        requestHashMap.put("checksum", "Generated from merchant backend");


Add Money integration has been completed successfully.


If your application uses ProGuard as well (and we think you should if you don't already), then please do specify the following rule to avoid integration issues:


    
    -keep class in.juspay.** {*;}
    -keep class in.freecharge.checkout.**{*;}
    -dontwarn in.juspay.**
    -dontwarn in.freecharge.checkout.**